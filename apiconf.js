/*
    Importable function to get the API Endpoint Security

    HOW TO IMPORT: 
    const { apiconf: apiconf } = require("./apiconf");

    HOW TO USE:
    let conf = apiconf(query,post_body,method);
*/

const SHA512 = require("crypto-js/sha512");
const hmacSHA512 = require("crypto-js/hmac-sha512");

const apiconf = function buildConf(
  query = {},
  post_body = "{}",
  method = "GET",
  path = ""
) {
  let timestamp = Date.now().toString();

  let url = "API_URL" + path;

  // query
  let query_sorted = Object.keys(query)
    .sort()
    .reduce((obj, key) => {
      obj[key] = query[key];
      return obj;
    }, {});

  let query_pretty_json = JSON.stringify(query_sorted, null, 1)
    .replace(/\n/g, "")
    .replace("{ ", "{");

  let body_pretty_json = null;

  if (post_body != "{}") {
    // body
    let body_sorted = Object.keys(post_body)
      .sort()
      .reduce((obj, key) => {
        obj[key] = post_body[key];
        return obj;
      }, {});

    body_pretty_json = JSON.stringify(body_sorted, null, 1)
      .replace(/\n/g, "")
      .replace("{ ", "{");
  } else {
    body_pretty_json = post_body;
  }

  let query_hash = SHA512(query_pretty_json).toString();

  let body_hash = SHA512(body_pretty_json).toString();

  let hmac = timestamp + method + url + query_hash + body_hash;
  let hmacDigest = hmacSHA512(hmac, "API_SECRET").toString();

  let config = {
    headers: {
      "X-API-KEY": "API_KEY",
      "X-GSMG-TIMESTAMP": timestamp,
      "X-GSMG-QUERY-HASH": query_hash,
      "X-GSMG-BODY-HASH": body_hash,
      "X-GSMG-SIGNATURE": hmacDigest,
    },
  };

  return config;
};

module.exports = { apiconf: apiconf };
