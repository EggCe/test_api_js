const { apiconf: apiconf } = require("./apiconf");
const axios = require("axios");

var api = class {
  async run() {
    let user_id = 12708;
    let base = "ANKR";
    let quote = "BNB";
    let exchange = "Binance";
    // let item = req.body.item;

    let method = "POST";
    // SORT BY KEYS ALPHABET =>> USE DOUBLE QUOTES
    let query = {
      base: "ANKR",
      exchange: "Binance",
      quote: "BNB",
      user_id: 12708,
    };

    let post_body = {
      Enable: true,
      "Trailing sell": true,
      "Trailing buy": true,
      "Surplus selling": false,
      "Allow buys above trailing sell": false,
      "Allocation percentage": 20,
      "Min trade profit percentage": "1.00",
      "Trade aggressiveness": 20,
      "Track optimised min trade profit": 1,
    };

    let path = "user_market_settings";
    let config = apiconf(query, post_body, method, path);

    let { data } = await axios
      .post(
        "API_URL" +
          "user_market_settings?user_id=" +
          user_id +
          "&exchange=" +
          exchange +
          "&base=" +
          base +
          "&quote=" +
          quote,
        post_body,
        config
      )
      .catch(function (error) {
        console.log(error);
      });

    console.log(data);
  }
};
var exec = new api();
exec.run();
